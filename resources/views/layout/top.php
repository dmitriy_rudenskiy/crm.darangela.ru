<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title><?= (empty($title)) ? 'Club' : $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/css/tron-ui.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">
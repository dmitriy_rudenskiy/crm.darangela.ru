<?php $title = 'Вход на сайт'; ?>

<?php include '../resources/views/layout/top.php' ?>

<?php include '../resources/views/layout/_menu.php' ?>

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">График производства</h4>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Заказчик</th>
                    <th>Наименование заказа</th>
                    <th>Тираж, шт.</th>
                    <th>Наименование загружаемой машины</th>
                    <th>Дата подачи заявки</th>
                    <th>Срок изготовления заказа, дни</th>
                    <th>График выполнения работ</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

<?php include '../resources/views/layout/button.php'; ?>